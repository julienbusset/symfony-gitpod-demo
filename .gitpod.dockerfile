FROM gitpod/workspace-mysql

# Install composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# Install symfony-cli
RUN sudo apt update
RUN sudo apt install ca-certificates
RUN echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | sudo tee /etc/apt/sources.list.d/symfony-cli.list
RUN sudo apt update
RUN sudo apt install symfony-cli

